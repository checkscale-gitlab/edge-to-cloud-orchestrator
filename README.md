# Edge-To-Cloud Resource Orchestration

&copy; Atos Spain S.A. 2020

[![License: Apache v2](https://img.shields.io/badge/License-Apache%20v2-blue.svg)](https://www.apache.org/licenses/LICENSE-2.0.html)

-----------------------

[Description](#description)

[Usage Guide](#usage-guide)

[LICENSE](#license)

-----------------------

### Description

The [PLEDGER](http://www.pledger-project.eu/) Edge-To-Cloud-Orchestrator application is a native-cloud Infrastructure-as-a-Service (IaaS) façade, written in Golang, which facilitates the deployment and life cycle management of containerized applications and serverless functions on container orchestration platforms like Docker, Kubernetes, Openshift etc.

It is composed by the following components / applications:

* E2CO application 
* Swagger REST API
* E2CO UI
* MongoDB

----------------------------

## Usage guide ##

### Kubernetes / from Docker Hub repositories ###

#### Installation ####

Applications:

* E2CO application 
* Swagger REST API
* E2CO UI
* MongoDB

**E2CO** and **Swagger REST API** applications are containerized in the same image, located in _atospledger/e2c-orchestrator_. The **E2CO UI** can be found in _atospledger/e2c-orchestrator-ui_. And finally, _MongoDB_ application comes in an other dockerized image (in the deployment YAML file is defined the location of the MongoDB application).

Images can be found on https://hub.docker.com/u/atospledger.

```
docker pull atospledger/e2c-orchestrator

docker pull atospledger/e2c-orchestrator-ui
```

##### deployment YAML #####

In folder '_kustomizer_' you can find the deployment YAML files used to deploy the application in different Kubernetes environments. This includes the **MongoDB**, the **E2CO UI** and the **E2CO** applications deployment.

File '_kustomizer/base/slalite-app.yaml_' contains a list of environment variables that need to be set in the case of Pledger context. These are the variables needed to connect to Kafka and other core components of the Pledger solution.

----------------------------

### Docker / from source code ###

#### Requirements

- docker

#### Installation

- clone respository

- go to folder _e2c-orchestrator_

- execute the following commands:

  ```bash
  docker build -t e2co .

  docker run -p 8333:8333 --name e2co e2co
  ```

##### Installation (golang)

###### Requirements

- golang

###### Installation

- clone respository

- go to folder _e2c-orchestrator_

- execute the following commands:

  ```bash
  go mod init atos.pledger/e2c-orchestrator
  go mod tidy
  go mod download

  go build

  go run main.go
  ```

###### Environment Variables

Variables that overwrite values defined in configuration files:

- E2CO_SLALiteEndPoint
- E2CO_Port
- E2CO_DB
- E2CO_DB_CONNECTION
- E2CO_DB_REPOSITORY
- E2CO_WARMING_TIME 
- E2CO_CONFIGSVC_ENDPOINT 
- E2CO_CONFIGSVC_CREDENTIALS 
- E2CO_KAFKA_CLIENT 
- E2CO_KAFKA_ENDPOINT 
- E2CO_KAFKA_VIOLATION_TOPIC 
- E2CO_KAFKA_CONFIG_TOPIC 
- E2CO_KAFKA_DEPLOY_TOPIC 
- E2CO_KAFKA_RESPONSE_TOPIC
- E2CO_KAFKA_KEYSTORE_PASSWORD 
- E2CO_KAFKA_KEY_PASSWORD 
- E2CO_KAFKA_TRUSTSTORE_PASSWORD 
- E2CO_KAFKA_KEYSTORE_LOCATION 
- E2CO_KAFKA_TRUSTSTORE_LOCATION 

-----------------------

### Usage Guide

**Swagger UI** (REST API interface): http://localhost:8333/swaggerui/

The REST API services exposed by the E2CO application are the following:

- /api/v1/ime
  Services used to manage the clusters / orchestrators managed by E2CO
  
- /api/v1/apps
  Applications managed by E2CO. These applications are deployed and launched in the clusters / orchestrators managed by the application.

#### Clusters definitions

This sections shows several examples of clusters definitions. To add a new cluster to the application users have to do a POST HTTP call to:

**POST** /api/v1/ime

  - body: [Cluster definitions](#cluster-definitions)

##### Cluster definitions

###### Kubernetes with connection token

```json
{
  "id": "cluster1",
  "name": "Cluster 1 - k8s",
  "description": "K8s cluster",
  "location": "Tenerife",
  "type": "k8s",
  "so": "ubuntu18",
  "defaultNamespace": "development",
  "restAPIEndPoint": "https://192.168.1.141:16443/",
  "ip": "192.168.1.141",
  "connectionToken": "eyJhbGciOiJSUzI1NiIsImtpZCI6IklxdWt0c01TY1R6TkdEWGsxcFlsT3BWcVlnVlJqT2U0VDNFcHd6UVVaZ1kifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJkZWZhdWx0LXRva2VuLXh0cXR2Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImRlZmF1bHQiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiJjY2NjZGE0OS02NGY1LTRkNDQtYWRjZi1kNjliM2E5YzRhMmEiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZS1zeXN0ZW06ZGVmYXVsdCJ9.PmBQyI-pN5mcSk-e20nnWKQ2jeR72WNjg_UD1mAfpWKBYPbyxs3MLYWVvwuj2zydYZsLkCvHQJSsrI5uMdPAZoc-2OPfV8pbRTGFDplX9us76R0WOv1_jCb0e6GhMI0jX4-OIXdcR2lC0VfQ5MfYcoVG16GMq6LWgw_Uxm4zGVqZ-QWw1CCg-QbkTTaoQ1h07w_2Dtp4QWKJr3evPj28XSn_PgwHzP-rWntOnMaMbRPMCyzri3Dq0ZAX4oZgAUWU87w99DexcX3rMPWhiu2E-hwmKkleJgbcn28xPDW2BHEncWUMUD38EIp3my9XUjd5H2awlmMIdn3NCoo1Jzdrtg",
  "slaLiteEndPoint": "",
  "prometheusPushgatewayEndPoint": "",
  "prometheusEndPoint": ""
}
```

###### MicroK8s without connection token

```json
{
  "id": "cluster2",
  "name": "Cluster 2 - microk8s",
  "description": "MicroK8s cluster",
  "location": "Tenerife",
  "type": "microk8s",
  "so": "ubuntu18",
  "defaultNamespace": "development",
  "restAPIEndPoint": "http://192.168.1.141:8001/",
  "ip": "192.168.1.141"
}
```

###### Docker without secure communication

```json
{
  "id": "cluster3",
  "name": "Cluster3",
  "description": "docker vm",
  "location": "Madrid",
  "type": "docker",
  "restAPIEndPoint": "tcp://192.168.1.133:2375",
  "ip": "192.168.1.133"
}
```

-----------------------

#### Application deployment:

This sections shows several examples of applicatoins definitions. To launch an application in one of the clusters managed by E2CO, users have to do a POST HTTP call to:

**POST** /api/v1/apps

  - body: [application definition](#application-definitions)

##### Applications definitions

- "Deploy one nginx application to location 'cluster1'" (tested with: **MicroK8s**, **Docker**)

```json
{
	"name": "nginx-app",
	"locations": [{
		"idE2cOrchestrator": "cluster1",
		"namespace": "development"
	}],
	"apps": [{
		"name": "nginx",
		"containers": [{
			"image": "nginx",
			"ports": [{
				"containerPort": 80,
				"protocol": "tcp"
			}]
		}],
        "service": {
            "expose": true
        }
	}]
}
```

```json
{
	"name": "nginx-app",
	"locations": [{
		"idE2cOrchestrator": "cluster1",
		"namespace": "development"
	}],
	"apps": [{
		"name": "nginx",
		"containers": [{
			"image": "nginx",
      "name": "nginx",
			"ports": [{
				"containerPort": 80,
				"protocol": "tcp"
			}],
      "hw": {
        "memory": "100Mi",
        "cpu": "25m"
      }
		}],
        "service": {
            "expose": true
        }
	}]
}
```

- containerized application (**K8s, microK8s, K3s**) with multiple pods and resources assignment

```json
{
  "name": "mult-app",
  "namespace": "development",
  "idE2cOrchestrator": "maincluster",
  "qos": [{
        "name": "throughput",
        "constraint": "throughput < 70000",
        "importance": [
            {
              "Name": "Mild",
              "Constraint": " < 70000"
            },
            {
              "Name": "Serious",
              "Constraint": " < 70"
            }],
        "penalties": [{
              "type": "",
              "value": "",
              "unit": ""
          }],
        "actions": [{
              "type": "",
              "value": "",
              "unit": ""
          }]
      },
      {
        "name": "responseTime",
        "constraint": "responseTime < 1053769"
      }],
  "replicas": 1,
  "containers": [
    {
      "name": "nginx",
      "image": "nginx",
      "ports": [
        {
          "containerPort": 80,
          "hostPort": 80,
          "protocol": "tcp"
        }
      ]
    }, 
    {
      "name": "memory-demo-ctr",
      "image": "polinux/stress",
      "resources": {
        "limits": {
          "memory": "200Mi"
        },
        "requests": { 
          "memory": "100Mi"
        }
      },
      "command": ["stress"],
      "args": ["--vm", "1", "--vm-bytes", "150M", "--vm-hang", "1"],
      "ports": []
    }
  ]
}
```

- containerized application (**K8s, microK8s, K3s**) - short version

```json
{
    "name": "nginx-app",
    "idE2cOrchestrator": "maincluster",
    "namespace": "core",
    "image": "nginx",
    "replicas": 1,
    "ports": [80],
    "qos": [{
        "name": "throughput",
        "constraint": "throughput < 70000",
        "importance": [
            {
              "Name": "Mild",
              "Constraint": " < 70000"
            },
            {
              "Name": "Serious",
              "Constraint": " < 70"
            }],
        "penalties": [{
              "type": "",
              "value": "",
              "unit": ""
          }],
        "actions": [{
              "type": "",
              "value": "",
              "unit": ""
          }]
      },
      {
        "name": "responseTime",
        "constraint": "responseTime < 1053769"
      }]
}
```

- containerized application (**K8s, microK8s, K3s**) - short version (version 2 - not implemented)

```json
{
    "name": "nginx-app",
    "idE2cOrchestrator": "maincluster",
    "namespace": "development",
    "qos": [
        "name": "throughput",
        "constraint": "throughput < 70000",
        "importance": [
            {
              "Name": "Mild",
              "Constraint": " < 70000"
            },
            {
              "Name": "Serious",
              "Constraint": " < 70"
            }],
        "penalties": [{
              "type": "",
              "value": "",
              "unit": ""
          }],
        "actions": [{
              "type": "",
              "value": "",
              "unit": ""
          }]
      },
      {
        "name": "responseTime",
        "constraint": "responseTime < 1053769"
      }],
    "images": [{"image": "nginx", "replicas": 2, "ports": [80]}]
}
```

- containerized application (**docker**)

```json
{
    "name": "nginx-app",
    "idE2cOrchestrator": "cluster3",
    "namespace": "core",
    "image": "nginx",
    "replicas": 1,
    "ports": [80],
    "qos": []
}
```


### LICENSE

`Edge-To-Cloud Resource Orchestration` is licensed under [Apache License, version 2](LICENSE.TXT).
